import React from 'react';
import { Grid } from 'semantic-ui-react';
import {
    BrowserRouter as Router,
  } from 'react-router-dom';

import 'semantic-ui-css/semantic.min.css'
import Picker from './components/Picker/Picker';
import Results from './components/Results/Results';

function App() {
    return (
        <Router>
            <div className="App">
                <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
                    <Grid.Column style={{ margin: '10em 0', maxWidth: 1280, backgroundColor: 'white' }}>
                        <>
                            <Picker />
                            <Results />
                        </>
                    </Grid.Column>
                </Grid>
        </div>
      </Router>
    );
}

export default App;
