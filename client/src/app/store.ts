import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import { database, picker } from './reducers';

export const store = configureStore({
  reducer: {
    database,
    picker
  }
});


export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
