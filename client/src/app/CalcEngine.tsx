import { Options } from '../components/Picker/Picker.interfaces';

export function hits(bs: number, options:Options): number {
    let result;

    result = 1 - (bs - 1) / 6;
  
    result = Math.min(5 / 6, result);
    result = Math.max(0, result);
  
    if (options) {
      if (options.bonus && options.bonus.hit) {
        result += options.bonus.hit / 6;
  
        result = Math.min(5 / 6, result);
        result = Math.max(0, result);
      }
  
      if (options.rerolls) {
        if (options.rerolls.hit === 'all') {
          result += (1 - result) * result;
        }
  
        if (options.rerolls.hit === '1s') {
          result += (1 / 6) * result;
        }
      }
    }
  
    if (result > 6) {
      return 0;
    }
  
    return result;
}

export function wounds(s: number, t: number, options: Options) {
    let result = 0;

    if (s / 2 >= t) {
      result = 5 / 6;
    } else if (s > t) {
      result = 4 / 6;
    } else if (s * 2 <= t) {
      result = 1 / 6;
    } else if (s < t) {
      result = 2 / 6;
    } else if (s === t) {
      result = 3 / 6;
    }

    result = Math.min(5 / 6, Math.max(0, result));
  
    if (options) {
      if (options.bonus.wound) {
        result += options.bonus.wound / 6;
  
        result = Math.min(5 / 6, Math.max(0, result));
      }
  
      if (options.rerolls.wound) {
        if (options.rerolls.wound === 'all') {
          result += (1 - result) * result;
        }
  
        if (options.rerolls.wound === '1s') {
          result += (1 / 6) * result;
        }
      }
    }
  
    return result;
  };

  export function saves(save: number) {
    if (save === 6) {
      return 1;
    }
  
    return (save - 1) / 6;
  };