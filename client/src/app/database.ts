import Dexie from 'dexie';

interface Result {
    name: string,
    attacks: number,
    hits: number,
    wounds: number,
    saved: number,
    damage: number,
    killed: number,
    cost: number
}

class WeaponDatabase extends Dexie {
    public weapons: Dexie.Table<Result, number>;

    public constructor() {
        super('WeaponDatabase');
        this.version(1).stores({
            weapons: "++id,name,attacks,hits,wounds,saved,damage,killed,cost,value"
        });
        this.weapons = this.table('weapons');
    }
}

const db = new WeaponDatabase();

export default db;