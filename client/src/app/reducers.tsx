import { PickerState, Weapon } from '../components/Picker/Picker.interfaces';
import {
    RECEIVE_WEAPONS,
    RECEIVE_UNITS,
    SET_UNIT_ATTACKER,
    SET_UNIT_DEFENDER,
    SET_PROFILES_ATTACKER,
    SET_PROFILES_DEFENDER,
    REMOVE_PROFILES_ATTACKER,
    SET_FACTION_ATTACKER,
    SET_FACTION_DEFENDER,
    ADD_WEAPON, 
    REMOVE_WEAPON,
    SET_OPTIONS
} from './types';

const databaseState: any = {
    units: {}
}

export function database(state = databaseState, action: any) {
    switch (action.type) {
        case RECEIVE_WEAPONS:
            return { ...state, weapons: action.weapons };
        case RECEIVE_UNITS:
            return { ...state, units: { ...state.units, [action.faction]: action.units }};
        default:
            return state;
    }
}

const pickerState: PickerState = {
    attacker: {
        name: '',
        faction: '',
        profiles: [],
        count: 0,
        options: {
            rerolls: {
                hit: false,
                wound: false
            },
            bonus: {
                hit: 0,
                wound: 0
            }
        },
        weapons: []
    },
    defender: {
        name: '',
        faction: '',
        profiles: [],
        options: {
            rerolls: {
                hit: false,
                wound: false
            },
            bonus: {
                hit: 0,
                wound: 0
            }
        },
    }
}

export function picker(state = pickerState, action: any) {
    switch (action.type) {
        case SET_FACTION_ATTACKER:
            return { ...state, attacker: { ...pickerState.attacker, faction: action.faction }};
        case SET_UNIT_ATTACKER:
            return { ...state, attacker: { ...state.attacker, weapons: pickerState.attacker.weapons, name: action.name }};
        case SET_PROFILES_ATTACKER:
            return { ...state, attacker: { ...state.attacker, profiles: action.profiles, count: Number(state.attacker.count) + 1 } };
        case REMOVE_PROFILES_ATTACKER:
            if (state.attacker.count > 0) {
                return { ...state, attacker: { ...state.attacker, count: Number(state.attacker.count) - 1 } };
            }
            return { ...state }
        case SET_FACTION_DEFENDER:
            return { ...state, defender: { ...pickerState.defender, faction: action.faction }};
        case SET_UNIT_DEFENDER:
            return { ...state, defender: { ...state.defender, name: action.name }};
        case SET_PROFILES_DEFENDER:
            return { ...state, defender: { ...state.defender, profiles: action.profiles } };
        case ADD_WEAPON:
            return { ...state, attacker: { ...state.attacker, weapons: [...state.attacker.weapons, action.weapon] } };
        case REMOVE_WEAPON:
            return { ...state, attacker: { ...state.attacker, weapons: [...state.attacker.weapons.filter((weapon: Weapon, index: number) => index !== action.index)] } }
        case SET_OPTIONS:
            return { ...state, attacker: { ...state.attacker, options: action.options } }    
        default:
            return state;
    }
}