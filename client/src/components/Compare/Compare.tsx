import React, { useEffect, useState } from 'react';
import { Table } from 'semantic-ui-react';

import { loadWeapons } from './actions';

const Compare = () => {
    const [weapons, setWeapons] =  useState([]);

    useEffect(() => {
        getWeapons();
    })

    function getWeapons () {
        loadWeapons().then((data: any) => {
            setWeapons(data);
        });
    }

    function renderSummary() {
        return (
            <React.Fragment>
                { weapons.map((weapon: any, index: number) => {
                    return (<Table.Row index={ index }>
                        { weapon.map((value: any|number, _index: number) => <Table.Cell key={ _index }>{ value }</Table.Cell>) }
                    </Table.Row>);
                }) }
            </React.Fragment>
        );
    }

    return (
        <Table>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Weapon</Table.HeaderCell>
                    <Table.HeaderCell>Attacks</Table.HeaderCell>
                    <Table.HeaderCell>Hits</Table.HeaderCell>
                    <Table.HeaderCell>Wounds</Table.HeaderCell>
                    <Table.HeaderCell>Saved</Table.HeaderCell>
                    <Table.HeaderCell>Damage</Table.HeaderCell>
                    <Table.HeaderCell>Killed</Table.HeaderCell>
                    <Table.HeaderCell>Cost</Table.HeaderCell>
                    <Table.HeaderCell>Value</Table.HeaderCell>
                    <Table.HeaderCell></Table.HeaderCell>
                </Table.Row>
            </Table.Header>

            <Table.Body>
                { renderSummary() }
            </Table.Body>
        </Table>
    );
};

export default Compare;