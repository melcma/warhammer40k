import db from '../../app/database';

export async function loadWeapons() {
    return await db.weapons.toArray();
}