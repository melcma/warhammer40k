import React, { Component } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { Button, Input, Table, Ref } from 'semantic-ui-react';

import { setProfilesAttacker, setProfilesDefender, removeProfilesAttacker } from './actions';

const mapStateToProps = (state: any) => ({
    attacker: state.picker.attacker,
    defender: state.picker.defender,
    units: state.database.units
})

const mapDispatchToProps = (dispatch: any) => ({
        setProfilesAttacker: (unit: any) => dispatch(setProfilesAttacker(unit)),
        setProfilesDefender: (unit: any) => dispatch(setProfilesDefender(unit)),
        removeProfilesAttacker: () => dispatch(removeProfilesAttacker())
})

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux & {}

class Profiles extends Component<Props> {
    defenderRow: any;

    constructor(props: Props) {
        super(props);

        this.defenderRow = React.createRef();
    }

    componentDidMount() {
        this.handleAddUnit(this.defenderRow.current, this.props.setProfilesDefender);
        this.handleRemoveUnit = this.handleRemoveUnit.bind(this);
    }

    handleAddUnit(row: any, handler: any) {
        const inputs = row.getElementsByTagName('input')
        const values = Array.prototype.map.call(inputs, (el: any) => Number(el.value));
        handler(values);
    }

    handleRemoveUnit() {
        this.props.removeProfilesAttacker();
    }

    renderUnit(unit: { faction: string, name: string }) {
        const { faction, name } = unit;

        const profiles = this.props.units[faction].find((profile: string) => profile[0] === name).slice(0, -1);

        return(
            profiles.map((stat: any, index: number) => {
                if (index === 0) {
                    return <Table.Cell key={index}>{stat}</Table.Cell>
                } else {
                    return <Table.Cell key={index}><Input defaultValue={stat} fluid transparent type='number' /></Table.Cell>
                }
            })
        );
    }

    render() {
        return(
            <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Name</Table.HeaderCell>
                        <Table.HeaderCell>WS</Table.HeaderCell>
                        <Table.HeaderCell>BS</Table.HeaderCell>
                        <Table.HeaderCell>S</Table.HeaderCell>
                        <Table.HeaderCell>T</Table.HeaderCell>
                        <Table.HeaderCell>W</Table.HeaderCell>
                        <Table.HeaderCell>A</Table.HeaderCell>
                        <Table.HeaderCell>SV</Table.HeaderCell>
                        <Table.HeaderCell>++</Table.HeaderCell>
                        <Table.HeaderCell>+++</Table.HeaderCell>
                        <Table.HeaderCell>Cost</Table.HeaderCell>
                        <Table.HeaderCell></Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    <Table.Row>
                        { this.renderUnit(this.props.attacker) }
                        <Table.Cell>
                            <Button as='div' fluid labelPosition='left'>
                                <Button.Group fluid>
                                    <Button primary onClick={(e: any) => this.handleAddUnit(e.target.parentNode.parentNode.parentNode.parentNode, this.props.setProfilesAttacker)}>
                                        Add
                                    </Button>
                                    <Button.Or text={ this.props.attacker.count }/>
                                    <Button onClick={this.handleRemoveUnit}>Remove</Button>
                                </Button.Group>
                            </Button>
                        </Table.Cell>
                    </Table.Row>
                    <Ref innerRef={this.defenderRow}>
                        <Table.Row>
                            { this.renderUnit(this.props.defender) }
                        </Table.Row>
                    </Ref>
                </Table.Body>
            </Table>
        );
    }
}

export default connector(Profiles);