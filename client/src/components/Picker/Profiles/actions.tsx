import {
    SET_PROFILES_ATTACKER,
    SET_PROFILES_DEFENDER,
    REMOVE_PROFILES_ATTACKER
} from '../../../app/types';;

export function setProfilesAttacker(profiles: any) {
    return {
        type: SET_PROFILES_ATTACKER,
        profiles
    }
}

export function setProfilesDefender(profiles: any) {
    return {
        type: SET_PROFILES_DEFENDER,
        profiles
    }
}

export function removeProfilesAttacker() {
    return {
        type: REMOVE_PROFILES_ATTACKER,
    }
}