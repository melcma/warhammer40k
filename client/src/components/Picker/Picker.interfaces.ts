export type Reroll = '1s' | 'failed' | 'all' | false;

export interface Profile {
    ws: number,
    bs: number,
}

export interface Options {
    rerolls: {
        hit: Reroll
        wound: Reroll
    },
    bonus: {
        hit: number,
        wound: number
    }
}

export interface Weapon {
    name: string,
    range: string,
    attacks: number,
    strength: number,
    ap: number,
    damage: number,
    cost: number,
    options: Options
}

export interface Attacker {
    name: string,
    faction: string,
    profiles: Array<string|number>,
    count: Number,
    options: Options,
    weapons: Array<Weapon>
}

export interface Defender {
    name: string,
    faction: string,
    profiles: Array<string|number>,
    options: Options
}

export interface PickerState {
    attacker: Attacker,
    defender: Defender
}

