import {
    SET_OPTIONS
} from '../../../app/types';

import { Options } from '../Picker.interfaces';

export function setOptions(options: Options) {
    return {
        type: SET_OPTIONS,
        options
    }
}