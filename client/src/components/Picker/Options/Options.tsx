import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { Dropdown, Input, Table } from 'semantic-ui-react';

import { Options as OptionsType } from '../Picker.interfaces';
import { setOptions } from './actions';

const rerollOptions = [
    { 
        key: 'none',
        text: 'none',
        value: false
     },
    { 
        key: '1s',
        text: '1s',
        value: '1s'
     },
     { 
        key: 'failed',
        text: 'failed',
        value: 'failed'
     },
     { 
        key: 'all',
        text: 'all',
        value: 'all'
     },
];

const mapDispatchToProps = (dispatch: any) => ({
    setOptions: (values: OptionsType) => dispatch(setOptions(values))
})

const connector = connect(null, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux & {}

class Options extends React.Component<Props, OptionsType> {
    constructor(props: Props) {
        super(props);

        this.state = {
            bonus: {
                hit: 0,
                wound: 0
            },
            rerolls: {
                hit: false,
                wound: false
            },
        };

        this.setHitBonus = this.setHitBonus.bind(this);
        this.setHitReroll = this.setHitReroll.bind(this);
        this.setWoundBonus = this.setWoundBonus.bind(this);
        this.setWoundReroll = this.setWoundReroll.bind(this);
    }

    setHitBonus(e: any) {
        this.setState({
            ...this.state,
            bonus: {
                ...this.state.bonus,
                hit: Number(e.target.value)
            }
        }, () => {
            this.props.setOptions(this.state);
        });
    }

    setHitReroll(e: any, { value }: any) {
        this.setState({
            ...this.state,
            rerolls: {
                ...this.state.rerolls,
                hit: value
            }
        }, () => {
            this.props.setOptions(this.state);
        });
    }

    setWoundBonus(e: any) {
        this.setState({
            ...this.state,
            bonus: {
                ...this.state.bonus,
                wound: Number(e.target.value)
            }
        }, () => {
            this.props.setOptions(this.state);
        });
    }

    setWoundReroll(e: any, { value }: any) {
        this.setState({
            ...this.state,
            rerolls: {
                ...this.state.rerolls,
                wound: value
            }
        }, () => {
            this.props.setOptions(this.state);
        });
    }

    HitTable() {
        return (
           <React.Fragment>
               <Table.Cell>
                    <Input type='number' fluid transparent defaultValue={0} onChange={this.setHitBonus} />
                </Table.Cell>
                <Table.Cell>
                    <Dropdown options={rerollOptions} defaultValue={rerollOptions[0].value} onChange={this.setHitReroll} />
                </Table.Cell>
            </React.Fragment>
        );
    }
    
    WoundTable() {
        return (
            <React.Fragment>
                <Table.Cell>
                     <Input type='number' fluid transparent defaultValue={0} onChange={this.setWoundBonus} />
                 </Table.Cell>
                 <Table.Cell>
                     <Dropdown options={rerollOptions} defaultValue={rerollOptions[0].value} onChange={this.setWoundReroll} />
                 </Table.Cell>
             </React.Fragment>
         );
    }
    
    TableHeader() {
        return(
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell colSpan='2'>Hit</Table.HeaderCell>
                    <Table.HeaderCell colSpan='2'>Wound</Table.HeaderCell>
                </Table.Row>
                <Table.Row>
                    <Table.HeaderCell>Roll Bonus</Table.HeaderCell>
                    <Table.HeaderCell>Reroll</Table.HeaderCell>
                    <Table.HeaderCell>Roll Bonus</Table.HeaderCell>
                    <Table.HeaderCell>Reroll</Table.HeaderCell>
                </Table.Row>
            </Table.Header>
        );
    }

    render() {
        return(
            <Table celled structured>
                { this.TableHeader() }
                <Table.Body>
                    <Table.Row>
                    { this.HitTable() }
                    { this.WoundTable() }
                    </Table.Row>
                </Table.Body>
            </Table>
        );
    };
}

export default connector(Options);