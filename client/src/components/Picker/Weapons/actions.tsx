import { ADD_WEAPON } from '../../../app/types';
import { Weapon } from '../Picker.interfaces';

export function addWeapon(weapon: Weapon) {
    return {
        type: ADD_WEAPON,
        weapon,
     }
}