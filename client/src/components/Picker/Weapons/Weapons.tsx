import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { Button, Input, Table } from 'semantic-ui-react';
import { addWeapon } from './actions';
import { Weapon } from '../Picker.interfaces'

const mapStateToProps = (state: any) => ({
    attacker: state.picker.attacker,
    defender: state.picker.defender,
    units: state.database.units,
    weapons: state.database.weapons,
})

const mapDispatchToProps = (dispatch: any) => ({
        addWeapon: (values: Weapon) => dispatch(addWeapon(values))
})

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux & {}

function Weapons (props: Props) {
    function collectWeapons() {
        const { faction, name } = props.attacker;
        const unit = props.units[faction];
        const data = unit && unit.find((el: any) => el[0] === name);
        const weapons = data && data[11];
        return weapons;
    }

    function handleAddWeapon(e: any) {
        const row = e.target.parentNode.parentNode;
        const name = row.getElementsByTagName('td')[0].innerHTML;
        const range = row.getElementsByTagName('td')[1].innerHTML;
        const inputs = row.getElementsByTagName('input')
        const values = Array.prototype.map.call(inputs, (el: any) => el.value);

        props.addWeapon({
            name,
            range,
            attacks: Number(values[0]),
            strength: Number(values[1]),
            ap: Number(values[2]),
            damage: Number(values[3]),
            cost: Number(values[4]),
            options: props.attacker.options
        });
    }

    function renderHeader() {
        return (<Table.Row>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>R</Table.HeaderCell>
            <Table.HeaderCell>A</Table.HeaderCell>
            <Table.HeaderCell>S</Table.HeaderCell>
            <Table.HeaderCell>AP</Table.HeaderCell>
            <Table.HeaderCell>D</Table.HeaderCell>
            <Table.HeaderCell>C</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
        </Table.Row>);
    }

    function renderWeapons() {
        const { weapons } = props;

        if (!weapons || !collectWeapons()) {
            return null;
        }

        const availableWeapons = weapons.filter((weapon: any) => collectWeapons().includes(weapon[0]));

        return(
            availableWeapons && availableWeapons.map((weapon: any, index: number) => {
                return <Table.Row key={index}>
                    { weapon.map((stat: any, _index: any) => {
                        if (_index < 2) {
                            return <Table.Cell key={_index}>{stat}</Table.Cell>
                        } else if (_index === 2) {
                            const type = weapon[1];
                            let attacks = Number(stat);

                            if (type === 'm') {
                                attacks += Number(props.attacker.profiles[5]);
                            }
                            return <Table.Cell key={_index}><Input defaultValue={attacks} fluid step='0.5' transparent type='number' /></Table.Cell>
                        } else if (_index === 5) {
                            return <Table.Cell key={_index}><Input defaultValue={stat} fluid step='0.5' transparent type='number' /></Table.Cell>
                        } else if (_index === 3) {
                            const type = weapon[1];
                            let strength = stat;
                            
                            if (type === 'm') {
                                if (strength === 'x2') {
                                    strength = Number(props.attacker.profiles[2]) * 2;
                                } else {
                                    strength = Number(strength) + Number(props.attacker.profiles[2]);
                                }
                            }
                            return <Table.Cell key={_index}><Input type='number' defaultValue={strength} fluid transparent /></Table.Cell>
                        } else {
                            return <Table.Cell key={_index}><Input type='number' defaultValue={stat} fluid transparent  /></Table.Cell>
                        }
                    }) }
                    <Table.Cell>
                        <Button primary fluid onClick={handleAddWeapon}>Add</Button>
                    </Table.Cell>
                </Table.Row>
            })
        );
    }

    return(
        <Table>
            <Table.Header>
                {renderHeader()}
            </Table.Header>

            <Table.Body>
                {renderWeapons()}
            </Table.Body>
        </Table>
    );
}

export default connector(Weapons);