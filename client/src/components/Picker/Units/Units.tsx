import React, { Component } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { Form, Grid } from 'semantic-ui-react';
import { setFactionAttacker, setFactionDefender, setUnitAttacker, setUnitDefender } from './actions';

const mapStateToProps = (state: any) => ({
        units: state.database.units,
        attacker: state.picker.attacker,
        defender: state.picker.defender
})

const mapDispatchToProps = (dispatch: any) => ({
        setFactionAttacker: (faction: string) => dispatch(setFactionAttacker(faction)),
        setFactionDefender: (faction: string) => dispatch(setFactionDefender(faction)),
        setUnitAttacker: (unit: string) => dispatch(setUnitAttacker(unit)),
        setUnitDefender: (unit: string) => dispatch(setUnitDefender(unit))
})

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux & {}
type State = {
    factions: Array<string>
    attacker: string,
    defender: string
}

class Units extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            factions: [
                'Space Marines',
                'Tau',
                'Chaos Space Marines',
                'Death Guard',
                'Drukhari'
            ],
            attacker: 'placeholder',
            defender: 'placeholder'
        }
        
        this.handleFactionAttackerChange = this.handleFactionAttackerChange.bind(this);
        this.handleFactionDefenderChange = this.handleFactionDefenderChange.bind(this);
        this.handleUnitAttackerChange = this.handleUnitAttackerChange.bind(this);
        this.handleUnitDefenderChange = this.handleUnitDefenderChange.bind(this);
    }

    handleFactionAttackerChange(e: React.ChangeEvent<HTMLSelectElement>) {
        const { setFactionAttacker } = this.props;
        const { value } = e.target;

        this.setState({ attacker: 'placeholder' });
        setFactionAttacker(value);
    }

    handleFactionDefenderChange(e: React.ChangeEvent<HTMLSelectElement>) {
        const { setFactionDefender } = this.props;
        const { value } = e.target;

        this.setState({ defender: 'placeholder' });
        setFactionDefender(value);
    }

    handleUnitAttackerChange(e: React.ChangeEvent<HTMLSelectElement>) {
        const { setUnitAttacker } = this.props;
        const { value } = e.target;

        this.setState({ attacker: value });
        setUnitAttacker(value);
    }

    handleUnitDefenderChange(e: React.ChangeEvent<HTMLSelectElement>) {
        const { setUnitDefender } = this.props;
        const { value } = e.target;

        this.setState({ defender: value });
        setUnitDefender(value);
    }

    renderFactions() {
        return this.state.factions.map((faction) => <option key={faction} defaultValue={faction}>{faction}</option>);
    }

    renderAttackerUnits() {
        return this.props.attacker.faction && this.props.units[this.props.attacker.faction].map((unit: any) => {
            return <option key={ unit[0] } defaultValue={unit[0]}>{ unit[0] }</option>;
        });
    }

    renderDefenderUnits() {
        return this.props.defender.faction && this.props.units[this.props.defender.faction].map((unit: any) => {
            return <option key={ unit[0] } defaultValue={unit[0]}>{ unit[0] }</option>;
        });
    }

    render() {
        return (
            <Form>
                <Grid centered columns={2}>
                    <Grid.Column textAlign='center' style={{ marginBottom: '1rem' }}>
                        <h3>Attacker</h3>
                    </Grid.Column>
                    <Grid.Column textAlign='center' style={{ marginBottom: '1rem' }}>
                        <h3>Defender</h3>
                    </Grid.Column>
                </Grid>
                <Form.Group widths='equal'>
                    <Form.Field>
                        <select name="faction_attacker" defaultValue="placeholder" onChange={this.handleFactionAttackerChange}>
                            <option disabled value="placeholder">Select faction</option>
                            {this.renderFactions()}
                        </select>
                    </Form.Field>
                    <Form.Field>
                        <select name="faction_defender" defaultValue="placeholder" onChange={this.handleFactionDefenderChange}>
                            <option disabled value="placeholder">Select faction</option>
                            {this.renderFactions()}
                        </select>
                    </Form.Field>
                </Form.Group>
                <Form.Group widths='equal'>
                    <Form.Field>
                        <select name="unit_attacker" disabled={!this.props.attacker.faction} onChange={this.handleUnitAttackerChange} value={this.state.attacker}>
                            <option disabled value="placeholder">Select unit</option>
                            {this.renderAttackerUnits()}
                        </select>
                    </Form.Field>
                    <Form.Field>
                        <select name="unit_defender" disabled={!this.props.defender.faction} onChange={this.handleUnitDefenderChange} value={this.state.defender}>
                            <option disabled value="placeholder">Select unit</option>
                            {this.renderDefenderUnits()}
                        </select>
                    </Form.Field>
                </Form.Group>
            </Form>)
    }
}

export default connector(Units);