import {
    SET_FACTION_ATTACKER,
    SET_FACTION_DEFENDER,
    SET_UNIT_ATTACKER,
    SET_UNIT_DEFENDER,
    RECEIVE_UNITS } from '../../../app/types';

import { apiUrl } from '../../../config';


function mapFactionNames(faction: string): string {
    const names: { [faction: string]: string } = {
        'Space Marines': 'space_marines',
        'Tau': 'tau',
        'Chaos Space Marines': 'chaos_space_marines',
        'Death Guard': 'death_guard',
        'Drukhari': 'drukhari'
    };

    return names[faction];
};

function setFaction(faction: string) {
    return fetch(`${apiUrl}/faction/${mapFactionNames(faction)}`)
    .then(res => res.json());
}

function receiveFactionAttacker(faction: string, units: string) {
    return async (dispatch: (Dispatch: any) => void) => {
        dispatch({
            type: RECEIVE_UNITS,
            faction,
            units
        });
        dispatch({
            type: SET_FACTION_ATTACKER,
            faction,
        });
    }
}

function receiveFactionDefender(faction: string, units: string) {
    return async (dispatch: (Dispatch: any) => void) => {
        dispatch({
            type: RECEIVE_UNITS,
            faction,
            units
        });
        dispatch({
            type: SET_FACTION_DEFENDER,
            faction,
        });
    }
}

export function setFactionAttacker(faction: string) {
    return async (dispatch: (Dispatch: any) => void) => {
        const data = await setFaction(faction);
        dispatch(receiveFactionAttacker(faction, data));
    }
}

export function setFactionDefender(faction: string) {
    return async (dispatch: (Dispatch: any) => void) => {
        const data = await setFaction(faction);
        dispatch(receiveFactionDefender(faction, data));
    }
}

export function setUnitAttacker(name: string) {
    return {
        type: SET_UNIT_ATTACKER,
        name
    }
}

export function setUnitDefender(name: string) {
    return {
        type: SET_UNIT_DEFENDER,
        name
    }
}