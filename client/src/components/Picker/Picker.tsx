import React, { Component } from 'react';
import { connect, ConnectedProps } from 'react-redux';

import Units from './Units/Units';
import Profiles from './Profiles/Profiles';
import Weapons from './Weapons/Weapons';
import Options from './Options/Options';
import { fetchWeapons } from './actions';

const mapStateToProps = (state: any) => ({
    attacker: state.picker.attacker,
    defender: state.picker.defender,
})


const mapDispatchToProps = (dispatch: any) => ({
    fetchWeapons: () => dispatch(fetchWeapons()),
})

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux & {}

class Picker extends Component<Props> {
    componentDidMount() {
        this.props.fetchWeapons();
    }

    render() {
        const { attacker, defender } = this.props;
        return (
            <React.Fragment>
                <Units />
                { (attacker.name && defender.name ) && <Profiles /> }
                { attacker.profiles.length ? <Weapons /> : null }
                { attacker.profiles.length ? <Options /> : null }
            </React.Fragment>
        )
    }
}

export default connector(Picker);