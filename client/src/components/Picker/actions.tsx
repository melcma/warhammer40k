import { RECEIVE_WEAPONS } from '../../app/types';
import { apiUrl } from '../../config';

function receiveWeapons(weapons: any) {
    return {
        type: RECEIVE_WEAPONS,
        weapons
    }
}

export function fetchWeapons() {
    return (dispatch: (Dispatch: any) => void) => {
        fetch(`${apiUrl}/weapons`)
        .then(res => res.json())
        .then(data => dispatch(receiveWeapons(data)))
    }
}