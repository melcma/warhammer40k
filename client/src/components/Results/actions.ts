import db from '../../app/database';

import {
    REMOVE_WEAPON
} from '../../app/types';

export function removeWeapon(index: number) {
    return {
        type: REMOVE_WEAPON,
        index
    }
}

export function saveResult(result: any) {
    db.weapons.add(result); 
}