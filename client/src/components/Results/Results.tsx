import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { Button, Icon, Table } from 'semantic-ui-react';

import { removeWeapon, saveResult } from './actions';
import { hits, wounds, saves } from '../../app/CalcEngine';

const toDecimal = (n: number) => Math.round(n * 100 + Number.EPSILON) / 100;

const mapStateToProps = (state: any) => ({
    attacker: state.picker.attacker,
    defender: state.picker.defender,
    weapons: state.picker.attacker.weapons
})

const mapDispatchToProps = (dispatch: any) => ({
    removeWeapon: (index: number) => dispatch(removeWeapon(index)),
    saveResult: (result: any) => dispatch(saveResult(result))
})

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;
type Props = PropsFromRedux & {}

function Results(props: Props) {
    function getHits(weaponType: string, attacks: number, options: any) {
        const skill = weaponType === 'm' ? attacker.profiles[0] : attacker.profiles[1];
        return toDecimal(hits(skill, options) * attacks);
    }

    function getWounds(hits: number, strength: number, options: any) {
        return toDecimal(wounds(strength, defender.profiles[3], options) * hits);
    }

    function getSaves(woundsChance: number, ap: number) {
        return toDecimal(saves(defender.profiles[6] - ap) * woundsChance);
    }

    function calculateValues() {
        return props.weapons.map((weapon: any) => {
            const hits = getHits(weapon.range, weapon.attacks, weapon.options);
            const woundsChance = getWounds(hits, weapon.strength, weapon.options);
            const savesChance = getSaves(woundsChance, weapon.ap);
            const damage = toDecimal(savesChance * weapon.damage);
            const kills = toDecimal(Math.min(savesChance, damage / defender.profiles[4]));

            return {
                name: weapon.name,
                attacks: weapon.attacks,
                hits,
                woundsChance,
                savesChance,
                damage,
                kills,
                cost: weapon.cost,
                value: '-'
            }
        });
    }
    
    function handleRemoveWeapon(index: number) {
        props.removeWeapon(index);
    }

    function renderRow() {
        return calculateValues().map((weapon: any, index: number) => {
            return (
                <Table.Row key={index}>
                    { Object.values(weapon).map((value: any, _index: number) => <Table.Cell key={_index}>{ value }</Table.Cell>) }
                    <Table.Cell>
                        <Button icon onClick={() => handleRemoveWeapon(index)}>
                            <Icon name='delete' />
                        </Button>
                    </Table.Cell>
                </Table.Row>
            );
        })
    }

    function renderSummary() {
        const totalCost = (value: number): number => toDecimal(Number(value) + Number(props.attacker.profiles[9]) * props.attacker.count)

        const summary = calculateValues()
            .map((v: any) => Object.values(v))
            .reduce((a: any, b: any) => a.map((c: any, i: any) => {
                if (i === 0) {
                    return c + ', ' + b[i];
                }

                return c + b[i];
            }))
            .map((v: any, index: number) => {
                if (index === 7) {
                    return totalCost(Number(v));
                }

                return v;
            });

        const calculateValue = toDecimal((summary[6] * props.defender.profiles[9]) / totalCost(summary[7]));

        const total = summary.map((value: string|number, index: number) => {
            if (index === 0) {
                return `${props.attacker.name} with ${value} vs ${props.defender.name}`;
            } else if (index === 8) {
                return calculateValue;
            } else {
                return toDecimal(Number(value));
            }
        })

        return (
            <Table.Row>
                { total.map((value: string|number, index: number) => {
                    if (index === 0) {
                        return<Table.Cell key={index}>Total</Table.Cell>
                    }

                    return <Table.Cell key={index}>{ value }</Table.Cell>;
                }) }
                <Table.Cell>
                    <Button icon onClick={() => saveResult(total)}>
                        <Icon name='delete' />
                    </Button>
                </Table.Cell>
            </Table.Row>
        );
    }

    const { attacker, defender, weapons } = props;
 
    if (!attacker.name || !defender.name || !weapons.length) {
        return null;
    }

    return(
        <Table>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Weapon</Table.HeaderCell>
                    <Table.HeaderCell>Attacks</Table.HeaderCell>
                    <Table.HeaderCell>Hits</Table.HeaderCell>
                    <Table.HeaderCell>Wounds</Table.HeaderCell>
                    <Table.HeaderCell>Saved</Table.HeaderCell>
                    <Table.HeaderCell>Damage</Table.HeaderCell>
                    <Table.HeaderCell>Killed</Table.HeaderCell>
                    <Table.HeaderCell>Cost</Table.HeaderCell>
                    <Table.HeaderCell>Value</Table.HeaderCell>
                    <Table.HeaderCell></Table.HeaderCell>
                </Table.Row>
            </Table.Header>

            <Table.Body>
                { renderRow() }
                { renderSummary() }
            </Table.Body>
        </Table>
    );
}

export default connector(Results);