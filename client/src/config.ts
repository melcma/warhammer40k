export const apiUrl = process.env.NODE_ENV === 'development' ? 'http://localhost:8006/api' : process.env.BACKEND_URL;
