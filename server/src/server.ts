import morgan from 'morgan';
import fs from 'fs';
import app from './app';
import { isDev } from './helpers';

const port = process.env.PORT || 8000;

const accessLogStream = fs.createWriteStream('logs/access.log', { flags: 'a' });

app.use(morgan('combined', { stream: accessLogStream }));

const server = app.listen(port, () => {
    if (isDev) console.log(`Listening in port ${port}`);
});

export default server;