import { CastingContext } from 'csv-parse';

export const weaponArrayCast = (value: string, context: CastingContext) => context.index === 11 ? value.split(',') : value;
export const isDev = process.env.NODE_ENV === 'development';