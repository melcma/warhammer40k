import router from './router';
import express from 'express';

const app = express();

const allowedDomains = [
    'http://localhost:8005',
    'http://www.adrianpiwowarczyk.com:8005',
    '*'
];
app.use((req, res, next) => {
    if (allowedDomains.includes(req.headers.origin)) {
        res.header('Access-Control-Allow-Origin', req.headers.origin);
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    };
    next();
});

app.use('/api', router);

export default app;
