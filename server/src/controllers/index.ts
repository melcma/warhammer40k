import faction from './faction';
import home from './home';
import weapons from './weapons';

const routes = {
  faction,
  home,
  weapons
};

export default routes;