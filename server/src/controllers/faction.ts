import * as fs from 'fs';
import { Request, Response } from 'express';
import parse from 'csv-parse/lib/sync';

import { weaponArrayCast } from '../helpers';

async function loadFactionFile(file: string): Promise<Buffer> {
    return new Promise((resolve, reject) => {
        fs.readFile(`data/factions/${file}.csv`, (err, data) => {
            if (err) reject(err);
            resolve(data);
        });
    });
}

async function faction (req: Request, res: Response): Promise<Response> {
    const { name } = req.params;
    try {
        const loadedData = await loadFactionFile(name);
        const factionData = parse(loadedData, { cast: weaponArrayCast });

        return res.send(factionData);
    } catch(err) {
        if (err.code === 'ENOENT') {
            return res
                .status(400)
                .send({status: 'error', message: 'Invalid faction, file not found'});
        }

        if (err.code === 'CSV_INCONSISTENT_RECORD_LENGTH') {
            return res
                .status(500)
                .send({status: 'error', message: 'Error when reading from faction file'});
        }

        return res.status(500).send({message: 'Unknown server error'});
    }
    
}

export default faction;