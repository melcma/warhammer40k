import request from 'supertest';
import express from 'express';
import controller from './home';

const app = express();
app.use('/', controller);

describe('home route', () => {
    it('returns 200 status', async () => {
        const response = await request(app).get('/');

        expect(response.status).toBe(200);
    });
})