import request from 'supertest';
import express from 'express';
import controller from './weapons';

const app = express();
app.use('/weapons', controller);

describe('weapons route', () => {
    it('returns weapons json', async () => {
        const response = await request(app).get('/weapons');

        expect(response.status).toBe(200);
        expect(response.body).not.toHaveLength(0);
    });
})