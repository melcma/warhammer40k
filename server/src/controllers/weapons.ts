import * as fs from 'fs';
import { Request, Response } from 'express';
import parse from 'csv-parse/lib/sync';

const csv = fs.readFileSync('data/weapons.csv');
const weapons = parse(csv);

export default (req: Request, res: Response): Response => res.send(weapons);