import request from 'supertest';
import express from 'express';
import routes from './index';

const app = express();

it('has 3 routes', () => {
    expect(Object.keys(routes)).toHaveLength(3);
})

describe('404 route', () => {
    it('returns 404', async () => {
        const response = await request(app).get('/unknown');

        expect(response.status).toBe(404);
    });
})