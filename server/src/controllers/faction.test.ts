import request from 'supertest';
import express from 'express';
import controller from './faction';

const app = express();
app.use('/faction/:name', controller);

describe('faction route', () => {
    it('returns Tau faction json', async () => {
        const response = await request(app).get('/faction/tau');

        expect(response.status).toBe(200);
        expect(response.body).not.toHaveLength(0);
    });

    it('returns error code 400 on incorrect faction', async () => {
        const response = await request(app).get('/faction/unknown');

        expect(response.status).toBe(400);
    });
})