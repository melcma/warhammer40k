import { Request, Response } from 'express';

export default (req: Request, res: Response): Response => res.sendStatus(200);