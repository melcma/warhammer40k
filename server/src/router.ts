import * as express from 'express';
import controllers from './controllers/index';

const router = express.Router();

router.get('/', controllers.home);
router.get('/weapons', controllers.weapons);
router.get('/faction/:name', controllers.faction);

export default router;