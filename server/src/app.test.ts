import request from 'supertest';
import app from './app'

describe('GET /unknown', () => {
    it('returns 404 page', async () => {
        const response = await request(app).get('/unknown');

        expect(response.status).toBe(404);
    })
})