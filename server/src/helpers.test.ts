import { CastingContext } from 'csv-parse';
import { weaponArrayCast, isDev } from './helpers';

describe('utility functions', () => {
    it('weaponArrayCast returns an array of weapons', () => {
        const weaponString = 'weapon1, weapon2, weapon3';
        const context: CastingContext = {
            column: 0,
            empty_lines: 0,
            header: false,
            index: 11,
            quoting: false,
            lines: 0,
            records: 0,
            invalid_field_length: 0
        };
        const weaponArray = weaponArrayCast(weaponString, context);
    
        expect(weaponArray).toHaveLength(3);
    });
    
    it('isDev returns false in test env', () => {
        expect(isDev).toBe(false);
    })
})